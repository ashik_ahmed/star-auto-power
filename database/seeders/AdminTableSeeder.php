<?php

namespace Database\Seeders;

use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'id' => 1,
            'name' => 'Admin',
            'phone_number' => '01911742233',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'email_verified_at' => '2022-03-15 10:28:34',
            'photo_path' => 'images/users/1727370207862851.webp',
            'remember_token' => '3ZYRLlFa9sz5LuNnLBf5Y5XT53uLjAwXey0wmLHpEquN7BbzRq264X4I68IL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
