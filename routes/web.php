<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/storage-link', function () {
    Artisan::call('storage:link');
});

// Admin route
Route::prefix('admin')->middleware('guest')->group(function (){
    Route::get('/login', [AdminController::class, 'login'])->name('admin.login');
    Route::post('/authenticate', [AdminController::class, 'authenticate'])->name('admin.authenticate');
});

Route::prefix('admin')->middleware('admin')->group(function (){

    Route::get('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('/profile', [AdminController::class, 'profile'])->name('admin.profile');
    Route::post('/profile/update', [AdminController::class, 'profile_store'])->name('admin.profile.update');
    Route::post('/change/password', [AdminController::class, 'change_password'])->name('admin.change.password');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');

    // Customer Route
    Route::get('/customer/all', [CustomerController::class, 'index'])->name('admin.customer.all');
    Route::post('/customer/store', [CustomerController::class, 'store'])->name('admin.customer.store');
    Route::get('/customer/edit/{id}', [CustomerController::class, 'edit'])->name('admin.customer.edit');
    Route::post('/customer/update/{id}', [CustomerController::class, 'update'])->name('admin.customer.update');
    Route::get('/customer/delete/{id}', [CustomerController::class, 'destroy'])->name('admin.customer.delete');

    // Slider Route
    Route::get('/slider/all', [SliderController::class, 'index'])->name('admin.slider.all');
    Route::post('/slider/store', [SliderController::class, 'store'])->name('admin.slider.store');
    Route::get('/slider/edit/{id}', [SliderController::class, 'edit'])->name('admin.slider.edit');
    Route::post('/slider/update/{id}', [SliderController::class, 'update'])->name('admin.slider.update');
    Route::get('/slider/delete/{id}', [SliderController::class, 'destroy'])->name('admin.slider.delete');

    // Posts route
    Route::get('/posts/all', [PostsController::class, 'index'])->name('admin.posts');
    Route::get('/posts/create', [PostsController::class, 'create'])->name('admin.posts.create');
    Route::post('/posts/store', [PostsController::class, 'store'])->name('admin.posts.store');
    Route::get('/posts/edit/{id}', [PostsController::class, 'edit'])->name('admin.posts.edit');
    Route::post('/posts/update/{id}', [PostsController::class, 'update'])->name('admin.posts.update');
    Route::get('/posts/delete/{id}', [PostsController::class, 'destroy'])->name('admin.posts.delete');

    // admin category route
    Route::get('/category', [CategoryController::class, 'index'])->name('admin.category');
    Route::post('/category/store', [CategoryController::class, 'store'])->name('admin.category.store');
    Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post('/category/update/{id}', [CategoryController::class, 'update'])->name('admin.category.update');
    Route::get('/category/delete/{id}', [CategoryController::class, 'dalete'])->name('admin.category.delete');

    Route::get('/service/create', [ServicesController::class, 'add_form'])->name('admin.service.create');
    Route::post('/service/store', [ServicesController::class, 'store'])->name('admin.service.store');
    Route::get('/service', [ServicesController::class, 'index'])->name('admin.service');
    Route::get('/service/edit/{id}', [ServicesController::class, 'edit'])->name('admin.service.edit');
    Route::post('/service/update/{id}', [ServicesController::class, 'update'])->name('admin.service.update');
    Route::get('/service/delete/{id}', [ServicesController::class, 'delete'])->name('admin.service.delete');

    Route::get('/order', [OrdersController::class, 'index'])->name('admin.order');
    Route::get('/order/new', [OrdersController::class, 'new_order'])->name('admin.order.new');
    Route::post('/order/store', [OrdersController::class, 'store'])->name('admin.order.store');
    Route::get('/order/edit/{id}', [OrdersController::class, 'edit'])->name('admin.order.edit');
    Route::post('/order/update/{id}', [OrdersController::class, 'update'])->name('admin.order.update');
    Route::get('/order/delete/{id}', [OrdersController::class, 'delete'])->name('admin.order.delete');
    Route::get('/order/pending', [OrdersController::class, 'pending'])->name('admin.order.pending');

    Route::get('/order/approved', [OrdersController::class, 'approved'])->name('admin.order.approved');
    Route::get('/order/approved/update/{id}', [OrdersController::class, 'approved_update'])->name('admin.order.approved.update');
    Route::get('/order/running', [OrdersController::class, 'running'])->name('admin.order.running');
    Route::get('/order/running/update/{id}', [OrdersController::class, 'running_update'])->name('admin.order.running.update');
    Route::get('/order/show/{id}', [OrdersController::class, 'show'])->name('admin.order.show');
    Route::get('/order/closed', [OrdersController::class, 'closed'])->name('admin.order.closed');
    Route::get('/order/print/{id}', [OrdersController::class, 'print'])->name('admin.order.print');
    Route::get('/order/pdf/{id}', [OrdersController::class, 'pdf_download'])->name('admin.order.pdf');

    // Orders Export route
    Route::get('/orders/export/', [OrdersController::class, 'export'])->name('admin.orders.export');
    Route::get('/orders/export/pending', [OrdersController::class, 'exportPending'])->name('admin.orders.export.pending');
    Route::get('/orders/export/approved', [OrdersController::class, 'exportApproved'])->name('admin.orders.export.approved');
    Route::get('/orders/export/running', [OrdersController::class, 'exportRunning'])->name('admin.orders.export.running');
    Route::get('/orders/export/closed', [OrdersController::class, 'exportClosed'])->name('admin.orders.export.closed');

    // Orders Export route
    Route::get('user/all', [UserController::class, 'index'])->name('admin.user.all');
    Route::get('user/create', [UserController::class, 'create'])->name('admin.user.create');
    Route::post('user/store', [UserController::class, 'store'])->name('admin.user.store');
    Route::get('user/edit/{id}', [UserController::class, 'edit'])->name('admin.user.edit');
    Route::post('user/update/{id}', [UserController::class, 'update'])->name('admin.user.update');
    Route::get('user/delete/{id}', [UserController::class, 'destroy'])->name('admin.user.delete');

    // Testimonial
    Route::get('/testimonial', [TestimonialController::class, 'index'])->name('admin.testimonial');
    Route::post('/testimonial/store', [TestimonialController::class, 'store'])->name('admin.testimonial.store');
    Route::get('/testimonial/edit/{id}', [TestimonialController::class, 'edit'])->name('admin.testimonial.edit');
    Route::post('/testimonial/update/{id}', [TestimonialController::class, 'update'])->name('admin.testimonial.update');
    Route::get('/testimonial/delete/{id}', [TestimonialController::class, 'delete'])->name('admin.testimonial.delete');

    Route::get('/settings/general', [SettingsController::class, 'general'])->name('admin.settings.general');
    Route::post('/settings/general/update', [SettingsController::class, 'general_update'])->name('admin.setting.general.update');
    Route::post('/settings/image_update', [SettingsController::class, 'image_update'])->name('admin.settings.image_update');

    Route::get('web-setting/about', [PagesController::class, 'about'])->name('admin.web-setting.about');
    Route::post('web-setting/about/store', [PagesController::class, 'about_store'])->name('admin.web-setting.about.store');
    Route::get('web-setting/terms', [PagesController::class, 'terms'])->name('admin.web-setting.terms');
    Route::post('web-setting/terms/store', [PagesController::class, 'terms_store'])->name('admin.web-setting.terms.store');

    Route::get('web-setting/privacy', [PagesController::class, 'privacy'])->name('admin.web-setting.privacy');
    Route::post('web-setting/privacy/store', [PagesController::class, 'privacy_store'])->name('admin.web-setting.privacy.store');

    Route::get('web-setting/circution', [PagesController::class, 'circution'])->name('admin.web-setting.circution');
    Route::post('web-setting/circution/store', [PagesController::class, 'circution_store'])->name('admin.web-setting.circution.store');

    Route::get('/contact', [ContactController::class, 'index'])->name('admin.contact');
    Route::get('/contact/pending', [ContactController::class, 'pending'])->name('admin.contact.pending');
    Route::get('/contact/closed', [ContactController::class, 'closed'])->name('admin.contact.closed');
    Route::get('/contact/edit/{id}', [ContactController::class, 'edit'])->name('admin.contact.edit');
    Route::post('/contact/update/{id}', [ContactController::class, 'update'])->name('admin.contact.update');
    Route::get('/contact/delete/{id}', [ContactController::class, 'delete'])->name('admin.contact.delete');
});


Route::group(['middleware' => ['guest', 'domain'] ], function(){
    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/register', [AuthController::class, 'register'])->name('register');
    Route::get('/forgot', [AuthController::class, 'forgot'])->name('forgot');
    Route::post('/send-password', [AuthController::class, 'send_password'])->name('forgot.password');
    Route::post('/send-otp', [AuthController::class, 'sendOtp'])->name('send.otp');
    Route::get('/verify-otp', [AuthController::class, 'verifyOtp'])->name('verify.otp');
    Route::post('/verify-phone', [AuthController::class, 'verify_phone_number'])->name('verify.phone');

});

// User route
Route::group(['middleware' => ['auth', 'domain'] ], function(){
    Route::get('/dashboard', [ProfileController::class, 'index'])->name('dashboard');
    Route::post('/profile/update/{id}', [ProfileController::class, 'profile_store'])->name('profile.update');
    Route::post('/change/password', [ProfileController::class, 'change_password'])->name('change.password');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::middleware('domain')->group(function (){
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/category/{slug}', [HomeController::class,'serviceByCategory'])->name('category.services');
    Route::get('/service/{slug}', [HomeController::class,'serviceDetails'])->name('service_detail');
    Route::get('/posts/{slug}', [HomeController::class,'single_post'])->name('single_post');
    Route::post('/order/store', [HomeController::class, 'order_store'])->name('order.store');

    Route::get('posts', [HomeController::class, 'posts'])->name('posts');

    Route::post('/subscribe/store', [HomeController::class,'subscribeStore'])->name('subscribe.store');

    Route::get('/contact', [HomeController::class,'contact'])->name('contact');
    Route::post('/contact/store', [HomeController::class,'contactStore'])->name('contact.store');

    Route::get('/about', [HomeController::class,'about'])->name('about');
    Route::get('/faq', [HomeController::class,'faq'])->name('faq');
    Route::get('/privacy-policy', [HomeController::class,'privacy_policy'])->name('privacy-policy');
    Route::get('/terms', [HomeController::class,'terms'])->name('terms');

});


