<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--title-->

    <!--favicon icon-->
    <link rel="icon" href="{{ asset(get_path('favicon.png')) }}" type="image/png">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/toastr/toastr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/toastr/ext-component-toastr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"/>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>

<body>
<!--preloader start-->
<div id="preloader">
    <div class="preloader-wrap">
        <img src="{{ asset(get_path('logo.png')) }}" alt="logo" class="img-fluid" />
    </div>
</div>
<!--preloader end-->

@include('layouts.header')

<div class="main">
    @yield('content')
</div>

@include('layouts.footer')

<script src="{{ asset('js/vendors/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('js/vendors/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ asset('js/vendors/jquery.easing.min.js') }}"></script>
<script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    @if(Session::has('success'))
        toastr.options = {
        "closeButton" : true,
        "progressBar" : true,
    }
    toastr.success('{{ session('success') }}', 'Congratulations!!!');
    @endif
        @if(Session::has('error'))
        toastr.options = {
        "closeButton" : true,
        "progressBar" : true
    }
    toastr.error('{{ session('error') }}', 'Ooops!!!');
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
        toastr.options = {
        "closeButton" : true,
        "progressBar" : true}
    toastr.error('{{ $error }}');
    @endforeach
    @endif

    $('.slider-item').slick({
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 3000,
        infinite: true,
    });

    $('.autoplay').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 3000,
        infinite: true,
    });
</script>

</body>
</html>


