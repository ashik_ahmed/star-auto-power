@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Create User')</h4>
                </div>
                <div class="card-body p-4">
                    <form action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-3">
                                    <label for="name" class="form-label">@lang('Name')</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter name">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="phone_number" class="form-label">@lang('Phone Number')</label>
                                    <input type="text" name="phone_number" class="form-control" placeholder="Enter phone">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="email" class="form-label">@lang('Email')</label>
                                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="image" class="col-form-label input-label">image</label>
                                    <input type="file" name="photo_path" class="form-control">
                                </div>
                            </div>
                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary btn-block">@lang('Submit')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
