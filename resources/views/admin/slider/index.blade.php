@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-7">
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-center table-hover datatable">
                            <thead class="thead-light">
                            <tr>
                                <th>@lang('Image')</th>
                                <th>@lang('Title')</th>
                                <th class="text-right">@lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sliders as $slider)
                                <tr>
                                    <td>
                                        <img src="{{ asset(get_path($slider->image)) }}" alt="" style="height: 40px">
                                    </td>
                                    <td>{{ $slider->title }}</td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.slider.edit', $slider->id) }}" class="btn btn-sm btn-white text-success me-2"><i class="lar la-edit"></i>@lang('Edit')</a>
                                        <a href="{{ route('admin.slider.delete', $slider->id) }}" class="btn btn-sm btn-white text-danger me-2"><i class="lar la-trash-alt"></i>@lang('Delete')</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Update Slider')</h4>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('admin.slider.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group mb-3">
                                        <label for="title" class="form-label">@lang('Title')</label>
                                        <input type="text" name="title" class="form-control" placeholder="Enter title">
                                        @error('title')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="description" class="form-label">@lang('Description')</label>
                                        <textarea class="form-control" name="description"></textarea>
                                        @error('description')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="image" class="form-label">@lang('Images 750x500')</label>
                                        <input type="file" name="image" class="form-control">
                                        @error('image')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="d-flex flex-wrap gap-2">
                                        <button type="submit" class="btn btn-primary">@lang('Add New')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection
