@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Update Slider')</h4>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('admin.slider.update', $slider->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group mb-3">
                                        <label for="title" class="form-label">@lang('Title')</label>
                                        <input type="text" name="title" class="form-control" value="{{ $slider->title }}">
                                        @error('title')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="description" class="form-label">@lang('Description')</label>
                                        <textarea class="form-control" rows="4" name="description">{!! $slider->description !!}</textarea>
                                        @error('description')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="image" class="form-label">@lang('Images 750x500')</label>
                                        <input type="file" name="image" class="form-control">
                                        @error('image')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="d-flex flex-wrap gap-2">
                                        <button type="submit" class="btn btn-primary">@lang('Add New')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->

        <div class="col-lg-5">
            <div class="img-thumbnail">
                <img src="{{ asset(get_path($slider->image)) }}" alt="" class="img-thumbnail">
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection
