@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Update Customer')</h4>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('admin.customer.update', $customer->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group mb-3">
                                        <label for="name" class="form-label">@lang('Name')</label>
                                        <input type="text" name="name" class="form-control" value="{{ $customer->name }}" placeholder="Enter name">
                                        @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="phone" class="form-label">@lang('Phone')</label>
                                        <input type="text" name="phone" class="form-control" value="{{ $customer->phone }}" placeholder="Enter phone">
                                        @error('phone')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="email" class="form-label">@lang('Email')</label>
                                        <input type="text" name="email" class="form-control" value="{{ $customer->email }}" placeholder="Enter email">
                                        @error('email')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="address" class="form-label">@lang('Address')</label>
                                        <textarea class="form-control" name="address">{!! $customer->address !!}</textarea>
                                        @error('address')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="d-flex flex-wrap gap-2">
                                        <button type="submit" class="btn btn-block btn-primary">@lang('Update Customer')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection
