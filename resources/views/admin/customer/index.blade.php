@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-7">
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-center table-hover datatable">
                            <thead class="thead-light">
                            <tr>
                                <th>@lang('Name')</th>
                                <th>@lang('Phone')</th>
                                <th>@lang('email')</th>
                                <th class="text-right">@lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.customer.edit', $customer->id) }}" class="btn btn-sm btn-white text-success me-2"><i class="lar la-edit"></i>@lang('Edit')</a>
                                        <a href="{{ route('admin.customer.delete', $customer->id) }}" class="btn btn-sm btn-white text-danger me-2"><i class="lar la-trash-alt"></i>@lang('Delete')</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Add Customer')</h4>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('admin.customer.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group mb-3">
                                        <label for="name" class="form-label">@lang('Name')</label>
                                        <input type="text" name="name" class="form-control" placeholder="Enter name">
                                        @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="phone" class="form-label">@lang('Phone')</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Enter phone">
                                        @error('phone')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="email" class="form-label">@lang('Email')</label>
                                        <input type="text" name="email" class="form-control" placeholder="Enter email">
                                        @error('email')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="address" class="form-label">@lang('Address')</label>
                                        <textarea class="form-control" name="address"></textarea>
                                        @error('address')<span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="d-flex flex-wrap gap-2">
                                        <button type="submit" class="btn btn-block btn-primary">@lang('Add Customer')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection
