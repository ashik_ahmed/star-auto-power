@extends('auth.layout')
@section('content')
    <div class="loginbox">
        <div class="login-right">
            <div class="login-right-wrap">
                <h1>@lang('Register')</h1>
                <p class="account-subtitle">@lang('Access to our dashboard')</p>
                <form action="{{ route('send.otp') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label">@lang('Enter name')</label>
                        <input type="text" name="name" class="form-control" value="Ashik Ahmed" placeholder="Enter Your Name">
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">@lang('Enter phone')</label>
                        <input type="number" name="phone_number" class="form-control" value="01516598533" placeholder="Enter phone number">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('Enter Password')</label>
                        <input type="password" name="password" class="form-control" value="password" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">@lang('Confirm Password')</label>
                        <input type="password" name="password_confirmation" class="form-control" value="password" placeholder="Confirm Password">
                    </div>
                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-lg btn-block btn-primary w-100">@lang('Register')</button>
                    </div>
                </form>

                <div class="login-or">
                    <span class="or-line"></span>
                    <span class="span-or">or</span>
                </div>
                <div class="text-center dont-have">@lang('Already have an account?') <a href="{{ route('login') }}">@lang('Login')</a></div>
            </div>
        </div>
    </div>
@endsection
