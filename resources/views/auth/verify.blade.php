@extends('auth.layout')
@section('content')

    <div class="loginbox">
        <div class="login-right">
            <div class="login-right-wrap">
                <h1>@lang('Verification Code')</h1>
                <p class="account-subtitle">@lang('Enter your email to get a password reset link')</p>
                <form action="{{ route('verify.phone') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label">@lang('Enter otp code')</label>
                        <input type="text" name="otp"  class="form-control" placeholder="Enter otp code">
                    </div>
                    <div class="form-group mb-0">
                        <button class="btn btn-lg btn-block btn-primary w-100" type="submit">@lang('Verify Code')</button>
                    </div>
                </form>
                <div class="text-center dont-have">@lang('Remember your password?') <a href="{{ route('login') }}">@lang('Login')</a></div>
            </div>
        </div>
    </div>

@endsection
