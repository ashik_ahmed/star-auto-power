@extends('auth.layout')
@section('content')

    <div class="loginbox">
        <div class="login-right">
            <div class="login-right-wrap">
                <h1>@lang('Forgot Your Password')</h1>
                <p class="account-subtitle">@lang('Enter your email to get a password reset link')</p>
                <form action="{{ route('forgot.password') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="phone_number">@lang('Enter phone number')</label>
                        <input type="text" name="phone_number"  class="form-control" placeholder="Enter phone number">
                    </div>
                    <div class="form-group mb-0">
                        <button class="btn btn-lg btn-block btn-primary w-100" type="submit">@lang('Reset Password')</button>
                    </div>
                </form>
                <div class="text-center dont-have">@lang('Remember your password?') <a href="{{ route('login') }}">@lang('Login')</a></div>
            </div>
        </div>
    </div>

@endsection
