@extends('auth.layout')
@section('content')

    <div class="loginbox">
        <div class="login-right">
            <div class="login-right-wrap">
                <h1>Login</h1>
                <p class="account-subtitle">Access to our dashboard</p>
                <form action="{{ route('admin.authenticate') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label">Enter email</label>
                        <input type="text" name="email" class="form-control" placeholder="Enter email">
                        @error('email')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Password</label>
                        <div class="pass-group">
                            <input type="password" name="password" class="form-control pass-input" placeholder="Enter password">
                            @error('password')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="cb1">
                                    <label class="custom-control-label" for="cb1">Remember me</label>
                                </div>
                            </div>
                            <div class="col-6 text-end">
                                <a class="forgot-link" href="#">Forgot Password ?</a>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-block btn-primary w-100" type="submit">Login</button>
                </form>
            </div>
        </div>
    </div>

@endsection
