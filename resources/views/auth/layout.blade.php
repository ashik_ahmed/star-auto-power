<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($title))
        <title>{{$title}}</title>
    @else
        <title>Login</title>
    @endif
    <link rel="shortcut icon" href="{{ asset('uploads/favicon.webp') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/ext-component-toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>

<body>
<div class="main-wrapper login-body">
    <div class="login-wrapper">
        <div class="container">
            <div class="text-center mb-4 w-25 m-auto">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong> {{ session::get('error') }} </strong>
                    </div>
                @endif

                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            <strong> {{ $error }} </strong>
                        </div>
                    @endforeach
                @endif
            </div>
            <a href="{{ url('/') }}">
                <img class="img-fluid logo-dark mb-2" src="{{ asset(get_path('logo.png')) }}" alt="Logo">
            </a>
            @yield('content')
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/script.js') }}"></script>
<script type="text/javascript">
    @if(Session::has('success'))
        toastr.options = {
        "closeButton" : true,
        "progressBar" : true,
    }
    toastr.success('{{ session('success') }}', 'Congratulations!!!');
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
        toastr.options = {
        "closeButton" : true,
        "progressBar" : true}
    toastr.error('{{ session('error') }}', 'Ooops!!!');
    @endforeach
    @endif
</script>
</body>
</html>
