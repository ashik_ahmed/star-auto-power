<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function login(Request $request){
        $request->validate([
            'phone_number' => 'required|exists:users,phone_number',
            'password' => 'required',
        ]);

        if (Auth::attempt(['phone_number' => $request->phone_number, 'password' => $request->password, 'isVerified' => true])) {
            // Role Check with redirect
            return redirect()->intended(RouteServiceProvider::HOME);
        }
        return redirect()->back()->with('success','Login details are not valid!');
    }


    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->back()->with('success','Logout successfully!');
    }

    public function forgot(){
        return view('auth.forgot');
    }

    public function send_password(Request $request){
        $request->validate([
            'phone_number' => ['required','exists:users,phone_number'],
        ]);
        $user = User::where('phone_number', $request->phone_number)->first();
        $password = rand(123456, 999999);
        $user->update([
            'password' => $password,
            sms_code_send($request->phone_number, 'Welcom to Star Auto Power, your password is : '.$password )
        ]);

        return redirect()->back()->with('success','Password send your phone number!');
    }


    public function register(Request $request){
        return view('auth.register');
    }

    public function sendOtp(Request $request){

        $request->validate([
            'phone_number' => ['required','unique:users', new PhoneNumber],
            'password' => ['required'],
        ]);

        $generate_otp = rand(123456, 999999);
        $user = new User();
        $user->name = $request->name;
        $user->phone_number = $request->phone_number;
        $user->otp = $generate_otp;
        $user->expire_at = Carbon::now()->addMinutes(60);
        $user->password = $request->password;
        $user->save();

        sms_code_send($request->phone_number, 'Welcom to Star Auto Power, your otp code is : '.$generate_otp);

        return redirect()->route('verify.otp')->with('success','Register successfully!');
    }

    public function verifyOtp(){
        return view('auth.verify');
    }

    public function verify_phone_number(Request $request){
        $request->validate([
            'otp' => 'required'
        ]);
        $otp_code   = User::where('otp', $request->otp)->first();
        if (!$otp_code) {
            return redirect()->back()->with('error', 'Your OTP is not correct');
        }elseif($otp_code && Carbon::now()->isAfter($otp_code->expire_at)){
            return redirect()->route('verify.otp')->with('error', 'Your OTP has been expired');
        }
        $otp_code->otp = null;
        $otp_code->expire_at = Carbon::now();
        $otp_code->isVerified = true;
        $otp_code->save();
        return redirect()->route('login')->with('success', 'Phone number verify success!');
    }

}
