<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $title = 'Manage Sliders';
        $sliders = Slider::latest()->get();
        return view('admin.slider.index', compact('title', 'sliders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'title' => ['required'],
            'description' => ['required'],
            'image' => ['required', 'mimes:jpeg,gif,bmp,png', 'max:2048'],
        ]);

        $data = new Slider();
        $data->title = $request->title;
        $data->description = $request->description;
        if ($request->hasFile('image')){
            $image = $request->file('image')->store('slider', 'public');
            Image::make(Storage::disk('public')->path($image))->resize(750, 500)->save();
            $data->image = $image;
        }
        $data->save();
        return redirect()->back()->with('success','Slider created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Update Slider';
        $slider = Slider::findOrFail($id);
        return view('admin.slider.update', compact('title', 'slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => ['required', 'string'],
            'description' => ['required'],
        ]);

        $data = Slider::findOrFail($id);
        $data->title = $request->title;
        $data->description = $request->description;
        if ($request->hasFile('image')){
            if (Storage::disk('public')->exists($data->image)) {
                Storage::disk('public')->delete($data->image);
            }
            $image = $request->file('image')->store('slider', 'public');
            Image::make(Storage::disk('public')->path($image))->resize(750, 500)->save();
            $data->image = $image;
        }
        $data->update();
        return redirect()->route('admin.slider.all')->with('success','Slider updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Slider::findOrFail($id);
        if(Storage::disk('public')->exists($data->image)){
            Storage::disk('public')->delete($data->image);
        }
        $data->delete();
        return redirect()->back()->with('success','Slider deleted successfully!');
    }
}
