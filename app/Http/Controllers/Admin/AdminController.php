<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function index(){
        $title = 'Dashboard';
        return view('admin.index', compact('title'));
    }

    public function profile(){
        $title = 'Admin Profile';
        $id = Auth::guard('admin')->user()->id;
        $user = User::find($id);
        return view('admin.profile.profile', compact('user', 'title'));
    }

    public function profile_store(Request $request){
        $id = Auth::guard('admin')->user()->id;
        $data = Admin::find($id);

        $data->name = $request->name;
        $data->phone_number = $request->phone_number;
        $data->email = $request->email;
        if ($request->hasFile('photo_path')){
            $image = $request->file('photo_path')->store('users', 'public');
            Image::make(Storage::disk('public')->path($image))->resize(150, 150)->save();
            $data->photo_path = $image;
        }
        $data->save();
        return redirect()->back()->with('success','data saved successfully');
    }

    public function change_password(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'password' => 'required|confirmed',
        ]);

        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->oldpassword,$hashedPassword)) {
            $user = User::find(Auth::id());
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();
            return redirect()->route('logout');
        }else{
            return redirect()->back()->with('error','data not updated successfully');
        }
    }
    public function login(){
        $title = 'Login';
        return view('auth.admin_login', compact('title'));
    }

    public function authenticate(Request $request){


        $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            // Role Check with redirect
            return redirect()->route('admin.dashboard')->with('success','you are successfully logged in!');
        }
        return redirect()->back()->with('success','Login details are not valid!');
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
