<?php


use Illuminate\Support\Carbon;

if (!function_exists('settings')) {
    function settings($key, $default=null){
        return \App\Models\Settings::getByKey($key, $default);
    }
}

if (!function_exists('dateFormat')){
    function dateFormat($value){
        return \Carbon\Carbon::parse($value)->format('d F, Y');
    }
}

if (!function_exists('slug') ) {
    function slug($val) {
        $slug = preg_replace('/\s+/u', '-', trim($val));
        return $slug;
    }
}

if (!function_exists('get_path') ) {
    function get_path($value) {
        return \Illuminate\Support\Facades\Storage::disk('public')->url($value);
    }
}


if (!function_exists('sms_code_send') ) {
    function sms_code_send($number='',$message=''){
        $url = "http://new.bulksmsbd.com/api/smsapi";
        $api_key = "LSKne9M9P8nTAhQQgWC1";
        $senderid = "03590002777";
        $number = $number;
        $message = $message;

        $data = [
            "api_key" => $api_key,
            "senderid" => $senderid,
            "number" => $number,
            "message" => $message,
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}

if (!function_exists('get_sms_balance') ) {
    function get_sms_balance(){
        $url = "http://new.bulksmsbd.com/api/getBalanceApi";
        $api_key = "LSKne9M9P8nTAhQQgWC1";
        $data = [
            "api_key" => $api_key
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}


if (!function_exists('short_text') ) {
    function short_text($string, $length = 50) {
        return Illuminate\Support\Str::limit($string, $length);
    }
}


if (!function_exists('short_desc') ) {
    function short_desc($string, $length = 200) {
        return Illuminate\Support\Str::limit($string, $length);
    }
}

if (!function_exists('diffForHumans') ) {
    function diffForHumans($date) {
        Carbon::setLocale(config('app.locale'));
        return Carbon::parse($date)->diffForHumans();
    }
}

if (!function_exists('showDateTime') ) {
    function showDateTime($date, $format = 'Y-m-d h:i A'){
        Carbon::setlocale(config('app.locale'));
        return Carbon::parse($date)->translatedFormat($format);
    }
}



/**
 * ===================================================================================================
 *  Send Single SMS
 * ===================================================================================================
 *
 * csms_id must be unique in same day
 */

$msisdn = "019XXXXXXXX";
$messageBody = "Message Body";
$csmsId = "2934fe343"; // csms id must be unique


echo singleSms($msisdn, $messageBody, $csmsId);


function singleSms($msisdn, $messageBody, $csmsId){
    $params = [
        "api_token" => '',
        "sid" => '',
        "msisdn" => $msisdn,
        "sms" => $messageBody,
        "csms_id" => $csmsId
    ];
    $url = "/api/v3/send-sms";
    $params = json_encode($params);
    echo callApi($url, $params);
}


function callApi($url, $params){
    $ch = curl_init(); // Initialize cURL
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($params),
        'accept:application/json'
    ));
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
